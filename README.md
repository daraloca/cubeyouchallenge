# README #

### Challenge tic-tac-toe ###

* A web-app implementing the game tic-tac-toe
* Version 0.1

### Set up and run ###
To run the web-app you need npm. Then:

- cd ./folder/to/the/project
- npm install
- npm start

Then the web-app will be available on http://localhost:8000
