'use strict';

angular.module('TTTChal.home')
.controller('HomeController', ['$state', function($state){

   var self = this;

   self.goPlay1vs1 = function() {
      $state.go('game.onevsone');
   };

}]);
