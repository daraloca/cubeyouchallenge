'use strict';

angular.module('TTTChal')
.directive('centeredElement', ['$timeout', '$document', function($timeout, $document) {

   function link(scope, element, attrs) {
      element.parent().css('height', '100%');
      element.parent().css('display', 'table');
      element.parent().css('margin', '0');
      element.parent().css('width', '100%');

      element.css('display', 'table-cell');
      element.css('padding', '0');
      element.css('margin', '0 auto');
      element.css('vertical-align', 'middle');
   }

   return {
      restrict: 'C',
      link: link
   };

}]);
