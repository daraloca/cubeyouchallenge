'use strict';

// Declare app level module which depends on views, and components
angular.module('TTTChal', [
   'ui.router',
   'TTTChal.version',
   'TTTChal.home',
   'TTTChal.game'
])
.config(['$urlRouterProvider', function($urlRouterProvider) {

   $urlRouterProvider.otherwise('/home');
}])
.run(['$rootScope', '$state', function($rootScope, $state) {

   $rootScope.$on('$stateChangeStart', function(evt, to, params) {
      if (to.redirectTo) {
         evt.preventDefault();
         $state.go(to.redirectTo, params, {location: 'replace'});
      }
   });

   /*$rootScope.$on('$locationChangeStart', function(evt, newUrl, oldUrl, newState, oldState) {
   });
   */

}]);
