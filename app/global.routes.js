'use strict';

angular
.module('TTTChal')
.config(function($stateProvider) {

   $stateProvider.state(
      {
         name: 'home',
         url: '/home',
         templateUrl: '/home/static/html/welcome.home.html'
      }
   )
   .state(
      {
         name: 'game',
         url: '/game',
         templateUrl: '/game/static/html/home.game.html',
         redirectTo: 'game.onevsone'
      }
   )
   .state(
      {
         name: 'game.onevsone',
         url: '/onevsone',
         templateUrl: '/game/static/html/onevsone.game.html'
      }
   );

});
