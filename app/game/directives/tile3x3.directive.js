'use strict';

angular.module('TTTChal.game')
.directive('tileThreeXThree', ['$timeout', '$document', function($timeout, $document) {

   function link(scope, element, attrs) {
      $timeout(function () {
         element.css('height', element.width());
         if(scope.positionX === 0) {
            element.css('border-top-color', 'transparent');
         }
         if(scope.positionX === scope.maxDimension - 1) {
            element.css('border-bottom-color', 'transparent');
         }
         if(scope.positionY === 0) {
            element.css('border-left-color', 'transparent');
         }
         if(scope.positionY === scope.maxDimension - 1) {
            element.css('border-right-color', 'transparent');
         }
      });
   }

   return {
      restrict: 'AE',
      scope: {
         positionX: '=',
         positionY: '=',
         maxDimension: '=',
         modelIn: '=',
         onWin: '&'
      },
      templateUrl: 'game/static/html/component/tile3x3.template.html',
      link: link,
      controller : ['$scope', function($scope) {
         var self = this;
      }]
   };

}]);
