'use strict';

angular.module('TTTChal.game')
.directive('boardDimension', ['$timeout', '$document', function($timeout, $window) {

   function link(scope, element, attrs) {
      $timeout(function () {
         if (element.css('max-width').replace('px', '') > $window.innerHeight) {
            element.css('max-width', $window.innerHeight - 50);
         }
      });
   }

   return {
      restrict: 'A',
      link: link
   };

}]);
