'use strict';

angular.module('TTTChal.game')
.controller('GameController', ['$state', '$scope', function($state, $scope){

   var self = this;

   self.init = function() {
      self.initVariables();
   };

   self.initVariables = function() {
      self.boardDimension = 3;
      self.initBoard();
      self.tileStyle = {
         "width": 100/self.boardDimension + "%"
      };
      self.turn = 0;
      self.pointsX = 0;
      self.pointsO = 0;
      self.nameX = 'X';
      self.nameO = 'O';
   };

   self.initBoard = function() {
      var allBoard = [];
      for(var i = 0; i < self.boardDimension; i++){
         var arr = new Array(self.boardDimension).fill(-1);
         allBoard.push(arr);
      }

      self.board = allBoard;
      self.blockBoard = false;
   };

   self.changeTurn = function(){
      self.turn = (self.turn + 1) % 2;
   };

   self.changeValue = function(x, y) {
      if (self.board[x][y] === -1 && !self.blockBoard) {
         self.board[x][y] = self.turn;
         self.changeTurn();
      }
   };

   self.controlWin = function() {
      //self.board;
      var win = false;
      //controls rows
      var winRow = self.controlArrayForWinning(self.board);

      //control cols
      var colTempArray = self.board.map(function(eli, i){
         return eli.map(function(elj, j) {
            return self.board[j][i];
         });
      });
      var winCol = self.controlArrayForWinning(colTempArray);

      //controls diagonal
      var diagTempArray = [];
      diagTempArray.push(self.board.map(function(eli, i){
         return self.board[i][i];
      }));
      var indexMax = self.boardDimension - 1;
      diagTempArray.push(self.board.map(function(eli, i){
         return self.board[indexMax - i][indexMax - i];

      }));
      var winDiag = self.controlArrayForWinning(diagTempArray);

      win = winRow || winCol || winDiag;
      if (win) {
         self.someOneWin();
      }
   };

   self.controlArrayForWinning = function(arrayIn) {
      return arrayIn.map(function(el){
         return el.reduce(function(a, b) {
            var notFull = true;
            if (Array.isArray(a)){
               notFull = b === -1 || a[1];
               return [a[0] + b, notFull];
            } else {
               notFull = b === -1 || a === -1;
               return [a + b, notFull];
            }
         });
      }).map(function(el){
         return (el[0] === 0 || el[0] === 3) && !el[1];
      }).reduce(function(a, b){
         return a || b;
      });
   };

   self.someOneWin = function() {
      self.blockBoard = true;
      if (self.turn % 2 === 0) {
         self.pointsO += 1;
      } else {
         self.pointsX += 1;
      }
   };

   $scope.$watch(function() {
      return self.turn;
   }, function(newValue, oldValue){
      self.controlWin();
   });

   self.init();

}]);
