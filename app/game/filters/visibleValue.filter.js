'use strict';

angular.module('TTTChal.game')
.filter('visibleValue', function() {
  return function(input) {
    if (input === -1) {
      return '';
   } else if (input % 2 === 0) {
      return 'X';
   } else if (input % 2 !== 0) {
      return 'O';
   }
    return input;
  };
});
