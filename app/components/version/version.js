'use strict';

angular.module('TTTChal.version', [
  'TTTChal.version.interpolate-filter',
  'TTTChal.version.version-directive'
])

.value('version', '0.1');
