'use strict';

describe('TTTChal.version module', function() {
  beforeEach(module('TTTChal.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
